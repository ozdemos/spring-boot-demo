# Spring Boot Demo



## K8s
* <https://spring.io/guides/gs/spring-boot-kubernetes/>


### prereqs
* jdk-11
* maven installed?

### build
  ~~~sh
  ./mvnw install
  ~~~

* check if runs
  ~~~sh
  java -jar target/*.jar
  # in another terminal
  curl localhost:8080/actuator | jq .
  # output
  {
    "_links": {
      "self": {
        "href": "http://localhost:8080/actuator",
        "templated": false
      },
      "health-path": {
        "href": "http://localhost:8080/actuator/health/{*path}",
        "templated": true
      },
      "health": {
        "href": "http://localhost:8080/actuator/health",
        "templated": false
      },
      "info": {
        "href": "http://localhost:8080/actuator/info",
        "templated": false
      }
    }
  }
  ~~~

* check if hello-worls works
~~~sh
 curl http://localhost:8080/hello-world?name=aaa | jq
 {
  "id": 1,
  "content": "Hello, aaa!"
}
~~~

* containerize
  ~~~
  ./mvnw spring-boot:build-image
  ~~~
* check if it runs
  ~~~
  docker run -p 8080:8080 demo:0.0.1-SNAPSHOT
  # in antoher terminal 
  curl localhost:8080/actuator/health
  ~~~

* push to a container registry
* first login
  ~~~
  ~~~
* tag image and push
  ~~~
  $ docker tag demo:0.0.1-SNAPSHOT <containerrepo>/demo
  $ docker push <containerrepo>/demo
  ~~~


## Docker

* <https://spring.io/guides/gs/spring-boot-docker/>

* how to build
~~~sh
mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)
./mvnw package && java -jar target/gs-spring-boot-docker-0.1.0.jar
~~~
~~~sh
docker build -t <>/gs-spring-boot-docker .
docker run -p 8080:8080 <>/gs-spring-boot-docker
~~~