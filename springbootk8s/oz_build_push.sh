#!/bin/bash
echo "SpringBoot example to OCP using UCD"

BUILD_VERSION=0.0.0

echo "Parameters are:"
while true; do
    case $1 in 
        -v | --version )
            BUILD_VERSION=$2; echo "Version for new build=$BUILD_VERSION"; shift 2 ;;
        -- ) echo "-- $1"; shift; break ;;
        * ) break ;;
    esac
done

# first step build with maven
./mvnw versions:set -DgroupID=com.example -DartifactId=springbootdemo -DnewVersion="${BUILD_VERSION}"

./mvnw install

# second step build container image, need input variable for version
docker build . -t springbootdemo:"${BUILD_VERSION}"
# ./mvnw spring-boot:build-image


docker tag springbootdemo:"${BUILD_VERSION}" quay.io/osmanburucuibm/springbootdemo:"${BUILD_VERSION}"

docker push quay.io/osmanburucuibm/springbootdemo:"${BUILD_VERSION}"
